import 'package:dio/dio.dart';
import 'package:koin/koin.dart';
import 'package:my_cookbook/blocs/auth_bloc.dart';
import 'package:my_cookbook/repositories/auth_repository.dart';
import 'package:my_cookbook/services/auth_service.dart';
import 'package:my_cookbook/services/custom_dio.dart';

final serviceModule = Module()
  ..single<Dio>((scope) => CustomDio().getApiDio())
  ..single((scope) => AuthService(scope.get()));

final repositoryModule = Module()
  ..single((scope) => AuthRepository(scope.get()));

final blocsModule = Module()..single((scope) => AuthBloc(scope.get()));
