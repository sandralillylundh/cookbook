import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_password_strength/flutter_password_strength.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:lottie/lottie.dart';
import 'package:my_cookbook/blocs/auth_bloc.dart';
import 'package:my_cookbook/blocs/utils.dart';
import 'package:my_cookbook/router/app_router.dart';

class LoginView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginView();
}

class _LoginView extends State<LoginView> {
  TextEditingController _mailControllerSignIn = TextEditingController();
  TextEditingController _pwdControllerSignIn = TextEditingController();

  TextEditingController _mailControllerCreate = TextEditingController();
  TextEditingController _pwdControllerCreate = TextEditingController();

  AuthBloc _authBloc;

  final _formKeySignIn = GlobalKey<FormState>();
  final _formKeyCreate = GlobalKey<FormState>();
  bool _hidePassword = true;
  PageController _pageController;

  bool _showError = false;
  String _errorMsg;

  @override
  void initState() {
    super.initState();
    _authBloc = get();
    _pageController = PageController();

    _mailControllerCreate.text = "sandralilllylundh@gmail.com";
    _pwdControllerCreate.text = "testing123456";

    _mailControllerSignIn.text = "sandralillylundh@gmail.com";
    _pwdControllerSignIn.text = "testing123456";

    _errorMsg = "";

    setListeners();
  }

  void setListeners() {
    _authBloc.user.listen((user) {
      print("HERE");
      if (user != null) {
        print(user);
        print("HERE");
        var route = AppRouter.generateRoute(RouteSettings(name: homeRoute));
        Navigator.of(context)
            .pushAndRemoveUntil(route, (Route<dynamic> route) => false);
      }
    });
    _authBloc.error.listen((error) {
      print(error);
      setState(() {
        _errorMsg = error;
      });
      _showError = true;
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void _togglePwd() {
    setState(() {
      _hidePassword = !_hidePassword;
    });
  }

  void _formRequestSignInValidation() {
    if (_formKeySignIn.currentState.validate()) {
      FocusScope.of(context).requestFocus(new FocusNode());
      var email = _mailControllerSignIn.text;
      var password = _pwdControllerSignIn.text;
      _authBloc.signIn(email, password);
    }
  }

  void _formRequestCreateValidation() {
    if (_formKeyCreate.currentState.validate()) {
      FocusScope.of(context).requestFocus(new FocusNode());
      var email = _mailControllerCreate.text;
      var password = _pwdControllerCreate.text;
      _authBloc.register(email, password);
    }
  }

  Widget _errorMessage() {
    return Visibility(
      visible: _showError,
      child: Text(_errorMsg),
    );
  }

  Widget _emailTextField(TextEditingController _controller) {
    return TextFormField(
      onTap: () {
        _showError = false;
      },
      controller: _controller,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: tr("email").capitalize() + '*',
      ),
      keyboardType: TextInputType.emailAddress,
      validator: (String value) {
        if (value.isEmpty) return tr("enter_email");
        if (!Utils.isValidEmailRegex.hasMatch(value))
          return tr("invalid_email");
        return null;
      },
    );
  }

  Widget _passwordTextField(TextEditingController _controller) {
    return TextFormField(
      onTap: () {
        _showError = false;
      },
      controller: _controller,
      decoration: InputDecoration(
        icon: Icon(Icons.vpn_key),
        labelText: tr("password").capitalize() + '*',
        suffixIcon: InkWell(
          splashColor: Colors.transparent,
          onTap: _togglePwd,
          child: Icon(
            _hidePassword ? Icons.visibility : Icons.visibility_off,
            size: 20.0,
            color: Colors.black,
          ),
        ),
      ),
      obscureText: _hidePassword,
      keyboardType: TextInputType.visiblePassword,
      validator: (String value) {
        if (value.isEmpty) return tr("enter_password");
        return null;
      },
    );
  }

  Widget _formValidationButton(String text, VoidCallback callback) {
    return RaisedButton(
      textColor: Colors.white,
      onPressed: callback,
      color: Colors.black,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
      child: Text(
        tr(text).toUpperCase(),
      ),
    );
  }

  Widget _header(String url) {
    return Lottie.network(url, width: 200, height: 200, repeat: false);
  }

  Widget _signInForm() {
    return Padding(
      padding: EdgeInsets.all(40),
      child: Column(
        children: [
          _emailTextField(_mailControllerSignIn),
          _passwordTextField(_pwdControllerSignIn),
          SizedBox(
            height: 40,
          ),
          _errorMessage(),
          _loading(),
          _formValidationButton("signin", _formRequestSignInValidation),
          GestureDetector(
            child: Text(tr("forgot_password").capitalize()),
          ),
        ],
      ),
    );
  }

  Widget _loading() {
    return StreamBuilder(
      stream: _authBloc.isLoading,
      builder: (_, AsyncSnapshot<bool> snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return Center(
            child: SizedBox(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(
                  strokeWidth: 3,
                )),
          );
        }
        return Container();
      },
    );
  }

  Widget _loginPage() {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            _header(
                "https://assets4.lottiefiles.com/packages/lf20_fefIZO.json"),
            Form(key: _formKeySignIn, child: _signInForm()),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                    onTap: () {
                      if (_pageController.hasClients) {
                        _pageController.animateToPage(
                          1,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeIn,
                        );
                      }
                    },
                    child: Text(tr("create_account").capitalize())),
                Icon(Icons.arrow_forward)
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _createForm() {
    return Padding(
      padding: EdgeInsets.all(40),
      child: Column(
        children: [
          _emailTextField(_mailControllerCreate),
          _passwordTextField(_pwdControllerCreate),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 40.0),
              child: FlutterPasswordStrength(
                password: _pwdControllerCreate.text,
                height: 3,
                width: 80,
              ),
            ),
          ),
          _loading(),
          _errorMessage(),
          _formValidationButton("create", _formRequestCreateValidation)
        ],
      ),
    );
  }

  Widget _createAccountPage() {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Container(
              color: Colors.white,
              child: _header(
                  "https://assets5.lottiefiles.com/packages/lf20_TmewUx.json"),
            ),
            Form(key: _formKeyCreate, child: _createForm()),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Icon(Icons.arrow_back),
                GestureDetector(
                    onTap: () {
                      if (_pageController.hasClients) {
                        _pageController.animateToPage(
                          0,
                          duration: const Duration(milliseconds: 500),
                          curve: Curves.easeIn,
                        );
                      }
                    },
                    child: Text("Log in")),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _pageController,
      children: [_loginPage(), _createAccountPage()],
    );
  }
}
