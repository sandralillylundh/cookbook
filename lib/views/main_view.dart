import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_cookbook/router/app_router.dart';
import 'package:my_cookbook/views/home_view.dart';
import 'package:my_cookbook/views/my_recipes.dart';

class MainView extends StatefulWidget {
  final List list = [HomeView(), MyRecipes()];
  final PageStorageBucket bucket = PageStorageBucket();
  @override
  State<StatefulWidget> createState() => _MainView();
}

class _MainView extends State<MainView> {
  int _selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          var route = AppRouter.generateRoute(RouteSettings(name: createRoute));
          Navigator.push(context, route);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
        elevation: 2.0,
      ),
      bottomNavigationBar: _bottomNavigationView(_selectedIndex),
      body: PageStorage(
        child: widget.list[_selectedIndex],
        bucket: widget.bucket,
      ),
    );
  }

  Widget _bottomNavigationView(int _selectedIndex) => BottomNavigationBar(
        onTap: (int index) => setState(() => _selectedIndex = index),
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: tr("home"),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: tr("my_recipes"),
          ),
        ],
        currentIndex: _selectedIndex,
        unselectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w700, fontSize: 8),
        selectedLabelStyle: TextStyle(
            color: Colors.black, fontWeight: FontWeight.w700, fontSize: 8),
        // selectedIconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        unselectedIconTheme: IconThemeData(color: Colors.grey),
        showUnselectedLabels: true,
        type: BottomNavigationBarType.fixed,
      );
}
