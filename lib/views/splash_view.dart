import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_cookbook/router/app_router.dart';

class SplashView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashView();
}

class _SplashView extends State<SplashView> {
  FlutterSecureStorage _storage = FlutterSecureStorage();

  @override
  void initState() {
    startProcess();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Future getCurrentUser() async {
    var user = await FirebaseAuth.instance.currentUser;
    return user;
  }

  void startProcess() {
    if (getCurrentUser() == null) {
      routeApp(loginRoute);
    }
    routeApp(homeRoute);
  }

  void routeApp(String path) async {
    var route = AppRouter.generateRoute(RouteSettings(name: path));
    await Future.delayed(Duration(seconds: 1));
    Navigator.of(context)
        .pushAndRemoveUntil(route, (Route<dynamic> route) => false);
  }
}
