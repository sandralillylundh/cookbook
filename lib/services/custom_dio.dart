import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class CustomDio {
  final _storage = FlutterSecureStorage();

  Dio getApiDio() {
    Dio dio = Dio();
    dio.interceptors.add(LogInterceptor(responseBody: true));
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) async {},
        onResponse: (Response response) async {
          return response;
        },
        onError: (DioError error) async {
          if (error.response?.statusCode == 401) {
            dio.interceptors.requestLock.lock();
            dio.interceptors.responseLock.lock();

            RequestOptions options = error.response.request;
            //TODO token refresh
            //String token = await _refreshMyToken(error.request.baseUrl);
            dio.interceptors.requestLock.unlock();
            dio.interceptors.responseLock.unlock();

            /* if (token != null) {
            options.headers["Authorization"] = "Bearer " + token;
            return dio.request(options.path, options: options);
          }*/
            // await _storage.delete(key: UserPreferences.accessToken.toString());
            // await _storage.delete(key: UserPreferences.refreshToken.toString());
          }
          return error;
        },
      ),
    );
    return dio;
  }

  Future<String> _refreshMyToken(String baseUrl) async {
    /* String accessToken;
    var refreshToken = await _storage.read(key: UserPreferences.refreshToken.toString());
    final response = await http.post(
        baseUrl + "oauth/v2/token",
        body: {
          "refresh_token": refreshToken,
          "grant_type": "refresh_token",
          "scope": "user",
          "client_id": environment["client_id"],
          "client_secret": environment["client_secret"]
        });
    if (response.statusCode == 200) {
      var json = jsonDecode(response.body);
      accessToken = json["access_token"];
      var refreshToken = json["refresh_token"];
      await _storage.write(key: UserPreferences.accessToken.toString(), value: accessToken);
      await _storage.write(key: UserPreferences.refreshToken.toString(), value: refreshToken);
    }
    return accessToken;*/
  }
}
