import 'package:firebase_auth/firebase_auth.dart';
import 'package:my_cookbook/base/error_messages.dart';
import 'package:my_cookbook/repositories/auth_repository.dart';
import 'package:rxdart/rxdart.dart';

class AuthBloc {
  AuthRepository _authRepository;

  AuthBloc(this._authRepository);

  final _errorSubject = BehaviorSubject<String>();
  Stream<String> get error => _errorSubject.stream;

  final _isLoadingSubject = BehaviorSubject<bool>();
  Stream<bool> get isLoading => _isLoadingSubject.stream;

  final _userSubject = BehaviorSubject<UserCredential>();
  Stream<UserCredential> get user => _userSubject.stream;

  void signIn(String email, String pwd) {
    _isLoadingSubject.add(true);
    _authRepository.signIn(email, pwd).then((value) {
      _userSubject.add(value);
    }).whenComplete(() {
      _isLoadingSubject.add(false);
    }).catchError((e) {
      _errorSubject.add(ErrorMessages(e.code));
    });
  }

  void register(String email, String pwd) async {
    _isLoadingSubject.add(true);

    _authRepository
        .register(email, pwd)
        .then((value) {
          _userSubject.add(value);
        })
        .whenComplete(() => _isLoadingSubject.add(false))
        .catchError((e) {
          _errorSubject.add(ErrorMessages(e.code));
        });
  }

  dispose() {
    _userSubject.close();
    _errorSubject.close();
    _isLoadingSubject.close();
  }
}
