import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_cookbook/services/auth_service.dart';

class AuthRepository {
  final AuthService _authService;
  final _storage = FlutterSecureStorage();

  AuthRepository(this._authService);

  Future<UserCredential> signIn(String email, String pwd) async {
    UserCredential userCredential = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: pwd);
    return userCredential;
  }

  Future<UserCredential> register(String email, String pwd) async {
    UserCredential userCredential = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: pwd);
    return userCredential;
  }

  //TODO login
}
