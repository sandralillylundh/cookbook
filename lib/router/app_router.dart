import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_cookbook/views/auth_view.dart';
import 'package:my_cookbook/views/create_recipe_view.dart';
import 'package:my_cookbook/views/main_view.dart';
import 'package:my_cookbook/views/splash_view.dart';

const String loginRoute = '/login';
const String splashRoute = '/spash';
const String homeRoute = '/home';
const String createRoute = '/create';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case loginRoute:
        return MaterialPageRoute(builder: (context) => LoginView());
      case splashRoute:
        return MaterialPageRoute(builder: (context) => SplashView());
      case homeRoute:
        return MaterialPageRoute(builder: (context) => MainView());
      case createRoute:
        return MaterialPageRoute(builder: (context) => CreateRecipeView());
      default:
        return MaterialPageRoute(builder: (context) => SplashView());
    }
  }
}
